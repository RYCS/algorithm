package day01;

import java.util.*;

/**
 * 两数之和
 *
 * @author 若雨成霜
 * <p>
 * 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那 两个 整数，并返回它们的数组下标。
 * <p>
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
 * <p>
 * 你可以按任意顺序返回答案。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[0,1]
 * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 * 示例 2：
 * <p>
 * 输入：nums = [3,2,4], target = 6
 * 输出：[1,2]
 * 示例 3：
 * <p>
 * 输入：nums = [3,3], target = 6
 * 输出：[0,1]
 */
public class Day01_1 {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSum(new int[]{2, 7, 11, 15}, 9)));
        System.out.println(Arrays.toString(twoSum(new int[]{3, 2, 4}, 6)));
        System.out.println(Arrays.toString(twoSum(new int[]{3, 3}, 6)));
    }

    public static int[] twoSum(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            throw new IllegalArgumentException("输入参数无效");
        }
        if (nums.length == 1 && target != nums[0]) {
            throw new IllegalArgumentException("输入参数无效");
        }
        Map<Integer, List<Integer>> valueMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int key = nums[i];
            List<Integer> list = valueMap.get(key);
            if (list == null) {
                list = new ArrayList<>();
                list.add(i);
                valueMap.put(key, list);
            } else {
                list.add(i);
            }
        }
        for (int source : nums) {
            int diff = target - source;
            List<Integer> list = valueMap.get(diff);
            if (list != null && diff == source && list.size() != 1) {
                return new int[]{valueMap.get(source).get(0), valueMap.get(source).get(1)};
            } else if (list != null && diff != source) {
                return new int[]{valueMap.get(source).get(0), valueMap.get(diff).get(0)};
            }
        }
        throw new RuntimeException("不存在这样的两个数值");
    }
}
